package facci.com.alexaherrera;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class BookListActivity extends AppCompatActivity {

    ListView listViewLibros;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_list);

        listViewLibros = findViewById(R.id.ListViewLibros);

        //ListView o RecyclerView
        List<Book> books = Book.listAll(Book.class);
        List<String> listaTextoLibros = new ArrayList<>();
        //foreach
        for (Book libro: books)
        {
            listaTextoLibros.add(
                    libro.getTitle() + " " + libro.getEdition());
        }
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(
                        this,
                        android.R.layout.simple_list_item_1,
                        listaTextoLibros);
        listViewLibros.setAdapter(itemsAdapter);
    }
}
