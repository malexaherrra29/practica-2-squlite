package facci.com.alexaherrera;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    Button ButtonGuardar, ButtonModificar,
            ButtonEliminar, ButtonConsultaGeneral,
            ButtonConsultaIndividual, ButtonEliminarTodo;
    ListView listViewLibros;
    EditText EditTextTitulo, EditTextEdicion, EditTextID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
        Book registro1 = new Book("Harry Potter",
                "Primera");
        registro1.save();
        Log.e("Guardar", "Datos guardados!");
*/
        EditTextID = findViewById(R.id.EditTextID);
        EditTextTitulo = findViewById(R.id.EditTextTitle);
        EditTextEdicion = findViewById(R.id.EditTextEdition);

        //CREATE
        ButtonGuardar = findViewById(R.id.ButtonGuardar);
        //READ
        ButtonConsultaGeneral = findViewById(R.id.ButtonConsultaGeneral);
        ButtonConsultaIndividual = findViewById(R.id.ButtonConsultaIndividual);
        //UPDATE
        ButtonModificar = findViewById(R.id.ButtonModificar);
        //DELETE
        ButtonEliminar = findViewById(R.id.ButtonEliminar);
        ButtonEliminarTodo = findViewById(R.id.ButtonEliminarTodo);

        ButtonConsultaIndividual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book book = Book.findById(
                        Book.class,
                        Long.parseLong(EditTextID.getText().toString()
                        ));
                EditTextTitulo.setText(book.getTitle());
                EditTextEdicion.setText(book.getEdition());
            }
        });


        ButtonConsultaGeneral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BookListActivity.class);
                startActivity(intent);
            }
        });

        ButtonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book registro1 = new Book(
                        EditTextTitulo.getText().toString(),
                        EditTextEdicion.getText().toString());
                registro1.save();
                Log.e("Guardar", "Datos guardados!");
            }
        });


        ButtonModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book book = Book.findById(Book.class, Long.parseLong("1"));
                book.title = "updated title here"; // edittext titulo
                book.edition = "3rd edition"; //edittext edicion
                book.save();
            }
        });


    }
}
