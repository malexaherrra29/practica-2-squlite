package facci.com.alexaherrera;

import com.orm.SugarRecord;

public class Registro extends SugarRecord<Registro> {

    String nombre;
    String apellido;

    public Registro() {

    }

    public Registro( String nombre, String apellido)
    {

        this.nombre = nombre;
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
}
